#!/bin/bash

# by Eriberto
# Create the manpage using txt2man command.

T2M_DATE="21 Nov 2018"
T2M_NAME=volatility
T2M_VERSION=2.6.1
T2M_LEVEL=1
T2M_DESC="advanced memory forensics framework"

# Don't change the following line
txt2man -d "$T2M_DATE" -t $T2M_NAME -r $T2M_NAME-$T2M_VERSION -s $T2M_LEVEL -v "$T2M_DESC" $T2M_NAME.txt > $T2M_NAME.$T2M_LEVEL
